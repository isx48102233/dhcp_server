# DHCP FAILOVER

The target is to get a test system with two dhcp servers (primary and secondary) that are serving the same pool to a client.

## VIRTUAL ENVIRONTMENT

This practice will be implemented using a KVM virtualization system under Fedora (any other linux system can be used).

### INSTALL KVM (libvirtd, virt-manager)

~~~ 
dnf -y install virt-manager
~~~

### CREATE AND INSTALL MAIN FEDORA QCOW2 TEMPLATE

Primer arrencarem el virt-manager.
~~~
virt-manager
~~~

Crearem una nova VM amb arrecada PXE ja que tenim l'instal·lació del fedora 24 per xarxa.
Configurem el tipus de SO en aquest cas Linux.
Deixem 2048 MiB de RAM i posem 2 CPU.
El disc el farem de 10 GiB.
Assignem un nom i començem l'instal·lació.
Ara seleccionem l'opció d'instal·lar fedora 24 i esperem fins que apareixi l'instal·lador

## CONFIG INSTAL·LACIÓ
Utilitzarem l'anglès com a llengua d'instal·lació.
Farem servir el teclat català ( borrem l'anglès ) i afegirem el català i castellà a llengues de suport.
Posarem Europe/Madrid com a "timezone".
A "Installation Source" desmarcarem la casella d'actualitzacions automàtiques.
A "Software selection" escollirem Fedora Server sense cap tipus d'extra.
Com ja he particionat disc, només hem de clickar a "Installation Destination" i prèmer Done per continuar.

#Començem l'instalació

Crearem l'usuari root amb la passwd del nostre planeta escollit i afegirem un usuari admin amb nom i passwd personals


### CREATE INCREMENTAL QCOW2 DISKS (dhcpd1, dhcpd2 and client)

# Comprovem el nom de l'imatge creada
~~~
cd /var/lib/libvirt/images/
ls
~~~
"fedora24.qcow2"

# Ara crearem 3 discos a partir d'aquest
#qemu-img create -f qcow nom_de_la_nova_imatge -b nom_de_la_imatge_referencia.qcow2

~~~
qemu-img create -f qcow dhcp_server1 -b fedora24.qcow2
qemu-img create -f qcow dhcp_server2 -b fedora24.qcow2
qemu-img create -f qcow client -b fedora24.qcow2
~~~
## IMPORTANT

# ARA HEM D'ESBORRAR LA VM ORIGINAL SENSE ESBORRAT L'IMATGE "fedora24.qcow2" PER EVITAR TRENCAR LA IMATGE ORIGINAL SENSE LA QUAL LA RESTA DE MAQUINES NO FUNCIONARÀ.

### CREATE SERVERS AND CLIENT KVM MACHINES



### CREATE AND CONFIGURE PRACTICE NETWORK ON VIRTUAL MACHINES

## DHCP SERVER 1

### NETWORK CONFIGURATION

### DHCPD SERVICE INSTALLATION

### DHCPD CONFIGURATION

## DHCP SERVER 2

### NETWORK CONFIGURATION

### DHCPD SERVICE INSTALLATION

### DHCPD CONFIGURATION

## DHCP CLIENT

### NETWORK CONFIGURATION

## TESTING ENVIRONTMENT


